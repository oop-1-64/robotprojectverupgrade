/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.robotproject_upgade;

/**
 *
 * @author domem
 */
public class Robot {

    private int x;
    private int y;
    private char symbol;
    private TableMap map;

    public Robot(char symbol, int x, int y, TableMap map) {
        this.x = x;
        this.y = y;
        this.symbol = symbol;
        this.map = map;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean walk(char direction) {
        switch (direction) {
            case 'w':
                if (walkW()) return false;
                break;
            case 's':
                if (walkS()) return false;
                break;
            case 'a':
                if (walkA()) return false;
                break;
            case 'd':
                if (walkD()) return false;
                break;
            default:
                return false;
        }
        if (map.isBomb(x, y)){
            System.out.println("Found bomb !! "+"("+getX()+", "+getY()+")");
        }
        return true;
    }

    private boolean walkD() {
        if (map.inMap(x + 1, y)) {
            x = x + 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkA() {
        if (map.inMap(x - 1, y)) {
            x = x - 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkS() {
        if (map.inMap(x, y + 1)) {
            y = y + 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkW() {
        if (map.inMap(x, y - 1)) {
            y = y - 1;
        } else {
            return true;
        }
        return false;
    }
    
    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }
}
