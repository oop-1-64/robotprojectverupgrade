/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.robotproject_upgade;

import java.util.Scanner;

/**
 *
 * @author domem
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner ip = new Scanner(System.in);
        TableMap map = new TableMap(10, 10);
        Robot robot = new Robot('R' ,2 ,2 ,map);
        Bomb bomb = new Bomb(5,5);
        map.setRobot(robot);
        map.setBomb(bomb);
        while(true){
            map.showMap();
            char direction = inputDirection(ip);
            if (direction == 'q'){
                printbyebye();
                break;
            }
            robot.walk(direction);
        }
        
        
    }

    private static void printbyebye() {
        System.out.println("Next time better luck");
    }

    private static char inputDirection(Scanner ip) {
        String choice = ip.next();
        return choice.charAt(0);
    }
    
}
